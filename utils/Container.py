from dependency_injector import containers, providers

from repositories.CityRepository import CityRepository
from repositories.GameRepository import GameRepository
from repositories.UserRepository import UserRepository
from services.GameService import GameService


class Container(containers.DeclarativeContainer):
  config = providers.Configuration()

  user_repository = providers.Factory(UserRepository)
  game_repository = providers.Factory(GameRepository)
  city_repository = providers.Factory(CityRepository)

  game_service = providers.Factory(
    GameService,
    game_repository=game_repository,
    user_repository=user_repository,
    city_repository=city_repository
  )


