class NewGameRequestDTO:
  def __init__(self, session_id: str, user_id: str) -> None:
    self.session_id = session_id
    self.user_id = user_id

  def get_session_id(self) -> str:
    return self.session_id

  def get_user_id(self) -> str:
    return self.user_id
