class NewUserCityRequestDTO:
  def __init__(self, session_id: str, city: str):
    self.session_id = session_id
    self.city = city

  def get_session_id(self):
    return self.session_id

  def get_city(self):
    return self.city
