class NewGameResponseDTO:
  def __init__(self, text: str) -> None:
    self.text = text

  def get_text(self) -> str:
    return self.text
