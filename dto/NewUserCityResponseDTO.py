class NewUserCityResponseDTO:
  def __init__(self, city: str, end: bool = False) -> None:
    self.city = city
    self.end = end

  def get_city(self):
    return self.city

  def get_end(self):
    return self.end
