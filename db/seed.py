import csv

from db import db_session
from db.db_session import create_session
from domain.City import City

cities = set()

with open('cities.csv', encoding='utf-8') as file:
  reader = csv.reader(file, delimiter=';')
  for row in reader:
    cities.add(row[3].lower())
print(len(cities))

db_session.global_init("alisa.sqlite")

session = create_session()

for city_name in cities:
  city = City(name=city_name)
  session.add(city)
session.commit()
session.close()
