from flask import Flask
from flask_restful import Api
from waitress import serve

from controllers.GameController import GameController
from db import db_session

app = Flask(__name__)
api = Api(app)


def main():
  db_session.global_init("./db/alisa.sqlite")
  api.add_resource(GameController, '/post')
  serve(app, host='0.0.0.0', port=5000)


if __name__ == '__main__':
    main()
