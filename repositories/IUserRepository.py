from abc import ABC, abstractmethod

from domain.User import User


class IUserRepository(ABC):
  @abstractmethod
  def add(self, user: User) -> User:
    pass

  @abstractmethod
  def find(self, id: str) -> User:
    pass
