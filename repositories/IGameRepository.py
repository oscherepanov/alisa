from abc import ABC, abstractmethod

from domain.Game import Game


class IGameRepository(ABC):
  @abstractmethod
  def add(self, game: Game) -> Game:
    pass

  @abstractmethod
  def find(self, game_id: str) -> Game:
    pass

  @abstractmethod
  def update(self, game: Game) -> Game:
    pass
