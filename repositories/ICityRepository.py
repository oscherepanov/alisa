from abc import ABC, abstractmethod
from typing import List

from domain.City import City


class ICityRepository(ABC):

  @abstractmethod
  def find_by_name(self, name: str) -> City:
    pass

  @abstractmethod
  def find(self, id: int) -> City:
    pass

  @abstractmethod
  def find_by_first_letter(self, letter: str) -> List[City]:
    pass

  @abstractmethod
  def get_last_city(self, game_id: str) -> City:
    pass


