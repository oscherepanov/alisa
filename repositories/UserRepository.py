from db.db_session import create_session
from domain.User import User
from repositories.IUserRepository import IUserRepository


class UserRepository(IUserRepository):
  def add(self, user: User) -> User:
    session = create_session()
    session.add(user)
    session.commit()
    session.close()
    return user

  def find(self, user_id: str) -> User:
    session = create_session()
    user = session.query(User).filter(User.id == user_id).first()
    session.close()
    return user
