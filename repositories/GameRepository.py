from db.db_session import create_session
from domain.Game import Game
from repositories.IGameRepository import IGameRepository


class GameRepository(IGameRepository):
  def add(self, game: Game) -> Game:
    session = create_session()
    session.add(game)
    session.commit()
    session.close()
    return game

  def find(self, game_id: str) -> Game:
    session = create_session()
    game = session.query(Game).filter(Game.id == game_id).first()
    session.close()
    return game

  def update(self, game: Game) -> Game:
    session = create_session()
    session.merge(game)
    session.commit()
    session.close()
    return game
