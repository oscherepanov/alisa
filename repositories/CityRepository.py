from typing import List

from db.db_session import create_session
from domain.GameCity import GameCity
from domain.City import City
from repositories.ICityRepository import ICityRepository


class CityRepository(ICityRepository):
  def find_by_name(self, name: str) -> City:
    session = create_session()
    city = session.query(City).filter(City.name == name).first()
    return city

  def find(self, id: int) -> City:
    session = create_session()
    city = session.query(City).filter(City.id == id).first()
    return city

  def find_by_first_letter(self, letter: str) -> List[City]:
    session = create_session()
    cities = session.query(City).filter(City.name.like(f"{letter}%")).all()
    return cities

  def get_last_city(self, game_id: str) -> City:
    session = create_session()
    city = session.query(City).join(GameCity.city).filter(GameCity.game_id == game_id).\
      order_by(GameCity.created_at.desc()).first()
    session.close()
    return city
