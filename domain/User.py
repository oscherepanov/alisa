from sqlalchemy import Column, Integer, String
from db.db_session import SqlAlchemyBase


class User(SqlAlchemyBase):
  __tablename__ = 'users'
  id = Column(String, primary_key=True)
