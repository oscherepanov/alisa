from datetime import datetime

from sqlalchemy import Integer, ForeignKey, Column, DateTime
from sqlalchemy.orm import relationship

from db.db_session import SqlAlchemyBase


class GameCity(SqlAlchemyBase):
  __tablename__ = 'game_cities'

  id = Column(Integer, primary_key=True, autoincrement=True)

  city_id = Column(Integer, ForeignKey('cities.id'), index=True)
  city = relationship('City', lazy='subquery')

  game_id = Column(Integer, ForeignKey('games.id'), index=True)

  created_at = Column(DateTime, default=datetime.utcnow)
