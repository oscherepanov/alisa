from sqlalchemy import Column, Integer, String

from db.db_session import SqlAlchemyBase


class City(SqlAlchemyBase):
  __tablename__ = 'cities'
  id = Column(Integer, primary_key=True, autoincrement=True)
  name = Column(String, index=True, nullable=True, unique=True)
