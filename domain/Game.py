from datetime import datetime

from sqlalchemy import Column, Integer, ForeignKey, DateTime, Table, String
from sqlalchemy.orm import relationship

from db.db_session import SqlAlchemyBase

class Game(SqlAlchemyBase):
  __tablename__ = 'games'

  id = Column(String, primary_key=True)

  user_id = Column(Integer, ForeignKey('users.id'), index=True)
  user = relationship('User', lazy='subquery')

  created_date = Column(DateTime, default=datetime.utcnow)

  game_cities = relationship('GameCity', lazy='subquery')
