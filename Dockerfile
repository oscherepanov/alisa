FROM python:3.8-slim-buster

ENV TZ=Asia/Yekaterinburg
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

COPY requirements.txt app/requirements.txt
WORKDIR /app

RUN pip3 install -r requirements.txt
COPY . /app

CMD [ "python3", "main.py"]
