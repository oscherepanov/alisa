from dependency_injector.wiring import inject, Provide
from flask import jsonify
from flask_restful import Resource, reqparse

from dto.NewGameRequestDTO import NewGameRequestDTO
from dto.NewUserCityRequestDTO import NewUserCityRequestDTO
from services.IGameService import IGameService
from utils.Container import Container


class GameController(Resource):

  @inject
  def __init__(self, game_service: IGameService = Container.game_service()):
    self.game_service = game_service

    self.parser = reqparse.RequestParser()
    self.parser.add_argument('request', type=dict, required=True)
    self.parser.add_argument('session', type=dict, required=True)
    self.parser.add_argument('version', type=str, required=True)

  def post(self):
    args = self.parser.parse_args()
    response = {
      'session': args['session'],
      'version': args['version'],
      'response': {
        'end_session': False
      }
    }
    user_id = args['session']['user_id']
    session_id = args['session']['session_id']
    if args['session']['new']:
      new_game_response = self.game_service.start_game(NewGameRequestDTO(session_id=session_id, user_id=user_id))
      response['response']['text'] = new_game_response.get_text()
    else:
      city_name = self.find_city_in_request(args['request'])
      if not city_name:
        city_name = args['request']['command']
      res = self.game_service.new_user_city(NewUserCityRequestDTO(session_id=session_id, city=city_name))
      response['response']['text'] = res.get_city()
      response['response']['end_session'] = res.get_end()
    return jsonify(response)

  def find_city_in_request(self, req):
    for entity in req['nlu']['entities']:
      if entity['type'] == 'YANDEX.GEO':
        return entity['value'].get('city', None)

