import random

from domain.GameCity import GameCity
from domain.City import City
from domain.Game import Game
from domain.User import User
from dto.NewGameRequestDTO import NewGameRequestDTO
from dto.NewGameResponseDTO import NewGameResponseDTO
from dto.NewUserCityRequestDTO import NewUserCityRequestDTO
from dto.NewUserCityResponseDTO import NewUserCityResponseDTO
from repositories.ICityRepository import ICityRepository
from repositories.IGameRepository import IGameRepository
from repositories.IUserRepository import IUserRepository
from services.IGameService import IGameService


class GameService(IGameService):

  def __init__(self, game_repository: IGameRepository,
               user_repository: IUserRepository,
               city_repository: ICityRepository) -> None:
    self.game_repository = game_repository
    self.user_repository = user_repository
    self.city_repository = city_repository

  def start_game(self, dto: NewGameRequestDTO) -> NewGameResponseDTO:
    user_id = dto.get_user_id()
    user = self.user_repository.find(user_id)
    if not user:
      user = User(id=user_id)
      user = self.user_repository.add(user)
    session_id = dto.get_session_id()
    game = Game(id=session_id, user=user)
    self.game_repository.add(game)
    return NewGameResponseDTO(text="Игра начилась. Чтобы прекратить игру скажите 'Сдаюсь'. Поехали. Назовите первый город.")


  def new_user_city(self, dto: NewUserCityRequestDTO) -> NewUserCityResponseDTO:

    if dto.city == "сдаюсь":
      return NewUserCityResponseDTO('У-ха-ха', True)


    session_id = dto.get_session_id()
    city_name = dto.get_city()

    # Validation

    game = self.game_repository.find(session_id)
    if city_name in [gc.city.name for gc in game.game_cities]:
      return NewUserCityResponseDTO('Этот город уже назывался')

    city = self.city_repository.find_by_name(city_name)
    if not city:
      return NewUserCityResponseDTO('Я не знаю такого города')

    last_city = self.city_repository.get_last_city(game_id=game.id)
    if last_city and last_city.name[-1] != city.name[0]:
      return NewUserCityResponseDTO(f'Город должен начинаться с буквы "{last_city.name[-1]}"')
    # -----------------

    game.game_cities.append(GameCity(game_id=game.id, city_id=city.id))
    self.game_repository.update(game)

    new_city = self.find_city(game, city_name)
    if not new_city:
      return NewUserCityResponseDTO('Я сдаюсь :(', True)

    game.game_cities.append(GameCity(game_id=game.id, city_id=new_city.id))
    self.game_repository.update(game)

    return NewUserCityResponseDTO(new_city.name.capitalize())

  def find_city(self, game: Game, city_name: str) -> City:
    last_latter = city_name[-1]
    if last_latter in 'ъь':
      last_latter = city_name[-2]

    new_cities = self.city_repository.find_by_first_letter(last_latter)

    cities = [gc.city_id for gc in game.game_cities]

    new_city = None

    while True:
      if len(new_cities) == 0:
        break
      city = random.choice(new_cities)
      if city.id not in cities:
        new_city = city
        break
      else:
        new_cities.remove(city)

    return new_city
