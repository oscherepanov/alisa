from abc import ABC, abstractmethod

from dto.NewGameRequestDTO import NewGameRequestDTO
from dto.NewGameResponseDTO import NewGameResponseDTO
from dto.NewUserCityRequestDTO import NewUserCityRequestDTO
from dto.NewUserCityResponseDTO import NewUserCityResponseDTO


class IGameService(ABC):

  @abstractmethod
  def start_game(self, dto: NewGameRequestDTO) -> NewGameResponseDTO:
    pass

  @abstractmethod
  def new_user_city(self, dto: NewUserCityRequestDTO) -> NewUserCityResponseDTO:
    pass
